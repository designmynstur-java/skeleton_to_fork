//------------------------------------------------------------------------------
// File Wizard.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Wizard implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Wizard(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("No way! Casting a spell on a wizard? Ha Ha!.\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("No use running. He will track you down.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Money is of no interest for a wizard.\n");
			return false;
		}
		if (pAct.typeStr.equals("ClimbAction")) {
			System.out.print("No. He is too old and will probably die.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("No! You cannot be that blood thirsty!.\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("The wizard says 'Have a nice day young man'.\n");
			return true;
		}
		System.err.print("Unknown action for Wall. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You meet with an old but mighty wizard.\n");
	}
}
