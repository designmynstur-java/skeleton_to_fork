//------------------------------------------------------------------------------
// File BargainAndBuyAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class BargainAndBuyAction extends Action {
	
	// constructor
	public BargainAndBuyAction() {
		super("BargainAndBuyAction", "Negotiate and offer money\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You are trying to talk and buy yourself out of it...");
            player.decResources();
		}
		else {
			System.out.println("Sorry. You're out of resources.");
            return false;
        }
        return true;
    }
}
