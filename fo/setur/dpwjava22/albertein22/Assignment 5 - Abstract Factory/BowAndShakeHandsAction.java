//------------------------------------------------------------------------------
// File BowAndShakeHandsAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class BowAndShakeHandsAction extends Action {
	
	// constructor
	public BowAndShakeHandsAction() {
		super("BowAndShakeHandsAction", "Bow politely and try to shake hands\n");
	}
		
	@Override
	public boolean doIt(Player player) {
		System.out.println("You are politely bowing and shaking hands...");
        return true;
    }
}
