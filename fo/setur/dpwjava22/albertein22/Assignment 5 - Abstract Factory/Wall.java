//------------------------------------------------------------------------------
// File Wall.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Wall implements Obstacle {	

	int delay = 2000;
	String type;

	// constructor
	public Wall(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;		
		System.out.print("--> ");		
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("You cannot cast a spell on a wall.\n");
			return false;
		}		
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("No use hiding from a wall.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("You cannot bargain with a wall.\n");
			return false;
		}
		if (pAct.typeStr.equals("ClimbAction")) {
			System.out.print("Sorry, the wall is too high.\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("How do you shake hands with a wall?.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Yes, the chainsaw gets you through!.\n");
			return true;
		}
		System.err.print("Unknown action for Wall. Terminating.\n");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> Now you are standing in front of a huge wall.\n");
	}
}
