//------------------------------------------------------------------------------
// File Dragon.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Dragon implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Dragon(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("SwordAction")) {
			System.out.print("The dragon blows a fire storm on you... You are badly hurt.\n");
			player.decHealth();
			return false;
		}
		if (pAct.typeStr.equals("RifleAction")) {
			System.out.print("That doesn't affect the armoured dragon much.\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("Running will only buy you some time.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Fatal. The dragon swallows both the chainsaw and you.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("He sure is not impressed.\n");
			return false;
		}
		if (pAct.typeStr.equals("OfferFoodAction")) {
			System.out.print("You can only feed a dragon with yourself.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Dragons don't use money.\n");
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("You are lucky! The dragon was instantly turned into a frog!\n");
			return true;
		}
		System.err.print("Unknown action for Dragon. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You encounter a red-eyed dragon with 10-inch teeth " +
       "and smoking nostrils.\n");
	}
}
