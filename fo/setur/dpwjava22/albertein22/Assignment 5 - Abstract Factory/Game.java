//------------------------------------------------------------------------------
// File Game.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
//                 code to be completed by student
//------------------------------------------------------------------------------

import java.util.ArrayList;
import java.util.Scanner;

public class Game {

	private String title;
	private ArrayList<Obstacle> obstacles;	// Obstacles to overcome in the game
	private ArrayList<Action> actions;  	// Possible actions the player may perform
	private Player player;
	
	public Game(GameFactory gFact) {  // gFact is a concrete factory-object
		/**
		* gFact points to a concrete Factory-object which needs to perform the operations.
     	*
     	* makeObstacles:
     	* Fill 'obstacles' with pointers to the game's Obstacle-objects!
     	*
     	* makeActions:
     	* Fill 'actions' with pointers to the Action-objects the player may choose from
     	*
     	* makePlayer:
     	* Assign 'player' a pointer to a concrete Player-object
     	*
     	* Give 'title' a suitable name
     	*/
	}

    public void play() {  // Play the game
    	System.out.println("*** Welcome to " + title + " ***");

    	// use for-each loop for conciseness
		for(Obstacle oit : obstacles) {
			int alt, choice;
			boolean passed = false;
			while (!passed && player.alive()) {
				oit.show();
				System.out.println();
				System.out.print("Choose action:\n");
				// use regular for loop due to complexity
				for(alt = 1; alt <= actions.size() && player.alive(); alt++) {
    				System.out.print("\t " + alt + " " + actions.get(alt-1).show());
				}
				Scanner input = new Scanner(System.in);
				choice = input.nextInt();
				
				passed = oit.tryToPass(player, actions.get(choice-1));
    			player.show();
    		}   // while
		}
		if (player.alive()) {
			System.out.println("\nYou Win!");
    	} else
    		System.out.println("\nYou Lose!");
    }
}
