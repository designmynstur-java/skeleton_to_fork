//------------------------------------------------------------------------------
// File SwordAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class SwordAction extends Action {
	
	// constructor
	public SwordAction() {
		super("SwordAction", "Use your sword\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		System.out.println("You are using your long sharp sword...");
        player.decHealth();
        return true;
    }
}
