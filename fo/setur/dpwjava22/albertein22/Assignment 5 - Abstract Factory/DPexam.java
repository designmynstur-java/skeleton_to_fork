//------------------------------------------------------------------------------
// File DPexam.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class DPexam implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public DPexam(String type) {
		this.type = type;
	}

	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("This will not help you pass.\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Brutal beast! Shame on you.\n");
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("Mambo-Jambo doesn't help you.\n");
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("Very nice, but not enough.\n");
			player.decHealth();
			return false;
		}
		if (pAct.typeStr.equals("ClimbAction")) {
			System.out.print("Hopeless.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Works every time...\n");
			return true;
		}
		System.err.print("Unknown action for Exam. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You have to pass an exam in Design patterns... \n");
	}
}
