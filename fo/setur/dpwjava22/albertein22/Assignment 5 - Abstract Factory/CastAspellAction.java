//------------------------------------------------------------------------------
// File CastAspellAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class CastAspellAction extends Action {
	
	// constructor
	public CastAspellAction() {
		super("CastAspellAction", "Try to cast a spell\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You are trying to cast a spell...");
			player.decResources();
		}
		else {
			System.out.println("Sorry. You're out of resources.");
            return false;
        }
        return true;
    }
}
