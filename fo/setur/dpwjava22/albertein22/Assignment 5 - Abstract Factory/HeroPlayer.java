//------------------------------------------------------------------------------
// File HeroPlayer.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class HeroPlayer extends Player {
	// constructor
	public HeroPlayer() {
		super("HeroPlayer", 7, 5, 5);
	}
}
