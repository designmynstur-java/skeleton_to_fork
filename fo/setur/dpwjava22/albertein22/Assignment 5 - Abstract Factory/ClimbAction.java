//------------------------------------------------------------------------------
// File ClimbAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class ClimbAction extends Action {
	
	// constructor
	public ClimbAction() {
		super("ClimbAction", "Try to climb\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You are trying to climb the obstacle...");
            player.decResources();
		}
		else {
			System.out.println("Sorry. You're out of resources.");
            return false;
        }
        return true;
    }
}
