//------------------------------------------------------------------------------
// File Gangster.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Gangster implements Obstacle {

	int delay = 2000;
	String type;

	// constructor
	public Gangster(String type) {
		this.type = type;
	}
	
	public boolean tryToPass(Player player, Action pAct) {
		if (!pAct.doIt(player))
			return false;
		System.out.print("--> ");
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (pAct.typeStr.equals("SwordAction")) {
			System.out.print("That was a big mistake. You are very dead.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("RifleAction")) {
			System.out.print("He was twice as fast as you... Sorry , you are dead.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("CastAspellAction")) {
			System.out.print("That doesn't impress the gun...\n");
			return false;
		}
		if (pAct.typeStr.equals("RunAndHideAction")) {
			System.out.print("BAM! He shot you in your back.\n");
			player.decHealth();
			return false;
		}
		if (pAct.typeStr.equals("OfferFoodAction")) {
			System.out.print("He is desperate, not hungry\n");
			return false;
		}
		if (pAct.typeStr.equals("ChainsawAction")) {
			System.out.print("Very unwise. He shot you right in your heart.\n");
			player.kill();
			return false;
		}
		if (pAct.typeStr.equals("BowAndShakeHandsAction")) {
			System.out.print("He is not impressed.\n");
			return false;
		}
		if (pAct.typeStr.equals("BargainAndBuyAction")) {
			System.out.print("Very clever. You bought yourself a passage.\n");
			return true;
		}
		System.err.print("Unknown action for Gangster. Terminating.");
		return false;
	}
	
	public void show() {
		System.out.print("\n>> You encounter a clean-cut gangster in a smart " +
       "suit with a gun in his hand.\n");
	}
}
