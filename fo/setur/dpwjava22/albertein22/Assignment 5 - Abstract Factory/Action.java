//------------------------------------------------------------------------------
// File Action.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public abstract class Action {
	protected String typeStr;
	protected String description;
	
	public Action(String type, String desc) {
		typeStr = type;
		description = desc;
	}

    public final String show() {return description;}
    public abstract boolean doIt(Player player);
}
