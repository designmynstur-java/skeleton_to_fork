//------------------------------------------------------------------------------
// File OfferFoodAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class OfferFoodAction extends Action {
	
	// constructor
	public OfferFoodAction() {
		super("OfferFoodAction", "Offer some food\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You are offering some deliciuos food...");
            player.decResources();
        } else {
        	System.out.println("Sorry. You're out of resources.");
            return false;
        }
        return true;
    }
}
