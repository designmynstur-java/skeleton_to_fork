//------------------------------------------------------------------------------
// File Main5Test.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
//                 code to be completed by student
//------------------------------------------------------------------------------

import java.util.Scanner;

public class Main5Test {
	public static void main(String[] args) {
		gameLoop();
	}
	
	public static void gameLoop() {
		int choice;
		boolean keepOn = true;
		
		while (keepOn) {
			GameFactory gf = null;
			System.out.println("Choose your game:");
			System.out.println("1 - Nice game");
			System.out.println("2 - Nasty game");
			System.out.println("3 - quit");
			
			Scanner input = new Scanner(System.in);
			choice = input.nextInt();
						
			switch (choice) {
				// case 1:  gf = A Factory object: break;
				// case 2:  gf = Another Factory object; break;
				case 3: keepOn = false;
				default: break;
			}
			
			if (keepOn) {
				// Game game = new Game(gf);
				// game.play();
			}
		} // while
	}
}
