//------------------------------------------------------------------------------
// File NiceGameFactory.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
//                 code to be completed by student
//------------------------------------------------------------------------------

import java.util.ArrayList;

public class NiceGameFactory implements GameFactory {

	/**
     * NiceGameFactory:
     *
     * Suitable operations
     * (implementations of GameFactory.cpp)
     *
     * Obstacles:
     * Wizard, Wall, Elephant, DPexam
     *
     * Possible Actions:
     * BowAndShakeHandsAction, RunAndHideAction, BargainAndBuyAction,
     * ChainsawAction, ClimbAction, CastAspellAction, SurrenderAction
     *
     * Player:
     * OrdinaryPlayer
     */
}