//------------------------------------------------------------------------------
// File OrdinaryPlayer.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class OrdinaryPlayer extends Player {
	// constructor
	public OrdinaryPlayer() {
		super("OrdinaryPlayer", 5, 3, 3);
	}
}
