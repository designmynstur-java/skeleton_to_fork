//------------------------------------------------------------------------------
// File Obstacle.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public interface Obstacle {

    void show();
    boolean tryToPass(Player player, Action action);
}
