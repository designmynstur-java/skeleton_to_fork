//------------------------------------------------------------------------------
// File ChainsawAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class ChainsawAction extends Action {
	
	// constructor
	public ChainsawAction() {
		super("ChainsawAction", "Use your chainsaw\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You are aggressivly using your chainsaw...");
			player.decResources();
		} 
		else {
			System.out.println("Sorry. You're out of resources.");
			return false;
		}
		return true;
	}

}
