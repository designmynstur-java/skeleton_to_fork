//------------------------------------------------------------------------------
// File RunAndHideAction.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class RunAndHideAction extends Action {
	
	// constructor
	public RunAndHideAction() {
		super("RunAndHideAction", "Run and try to hide\n");
	}
	
	@Override
	public boolean doIt(Player player) {
		if (player.getResources() > 0) {
			System.out.println("You think running and hiding will save you...");
            player.decResources();
		}
        else {
        	System.out.println("Sorry. You're out of resources.");
            return false;
        }
        return true;
    }
}
