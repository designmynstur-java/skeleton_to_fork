//------------------------------------------------------------------------------
// File Player.java for Assignment 5 - Abstract Factory
// Course: Design Patterns with Java (5041.22)
// Hilmar Simonsen, 16/2-2022
//------------------------------------------------------------------------------

public class Player {
	private String typeStr;
	private int resources, ammo, health;
	
	public Player(String type, int r, int a, int h) {
		typeStr = type;
		resources = r;
		ammo = a;
		health = h;
	}
	
	public void decResources() { --resources; }
	public void incResources() { ++resources; }
	public final int getResources() { return resources; }
	public void decAmmo() { --ammo; }
	public final int getAmmo() { return ammo; }
	public void decHealth() { --health; }
	public final int getHealth() { return health; }
	public void kill() { health = 0; }
	public final boolean alive() { return health > 0; }
	public void show() {
		System.out.println("\nStatus health:" + health + 
				" resources:" + resources + " ammo:" + ammo);
	}
}
