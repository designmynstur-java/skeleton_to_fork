// Simple DP Strategy test

public class Main {

	public static void main(String[] args) {
		// Class Robot instantiated three times (three objects created)
		System.out.println();
		Robot r1 = new Robot("Big Robot");
		Robot r2 = new Robot("George v.2.1");
		Robot r3 = new Robot("R2");

		// Robot method called with ConcreteStrategies as arguments
		r1.setBehaviour(new AgressiveBehaviour());
		r2.setBehaviour(new DefensiveBehaviour());
		r3.setBehaviour(new NormalBehaviour());

		// how the robots are going to move
		r1.move();
		r2.move();
		r3.move();

		System.out.println("\r\nNew behaviours: " +
				"\r\n\t'Big Robot' gets really scared, " +
				"\r\n\t'George v.2.1' becomes really mad because " +
				"it's always attacked by other robots, " +
				"\r\n\t and R2 keeps itself calm\r\n");

		// Robot method called with new ConcreteStrategies as arguments ('let the algorithms vary independently from clients that use it')
		r1.setBehaviour(new DefensiveBehaviour());
		r2.setBehaviour(new AgressiveBehaviour());

		r1.move();
		r2.move();
		r3.move();

		System.out.println();
	}
}
