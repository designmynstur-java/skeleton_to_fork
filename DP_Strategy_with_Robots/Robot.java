// This is the Context

public class Robot {
	// instance variables
	IBehaviour behaviour;
	String name;

	// constructor
	public Robot(String name) {
		this.name = name;
	}

	// setters
	public void setBehaviour(IBehaviour behaviour) {
		this.behaviour = behaviour;
	}

	public void setName(String name) {
		this.name = name;
	}

	// getters
	public IBehaviour getBehaviour() {
		return behaviour;
	}

	public String getName() {
		return name;
	}

	// how the robots are going to move
	public void move() {
		System.out.println(this.name + ": Based on current position " +
					 "the behaviour object decides the next move: ");

		// ... send the command to mechanism
		int command = behaviour.moveCommand();
		
		System.out.println("\tCommand sent to the mechanism: " + command);
		System.out.println("\tThe result returned by behaviour object " +
					"is sent to the movement mechanisms " + 
					"for the robot '"  + this.name + "'");
	}
}
